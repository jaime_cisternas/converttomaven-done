package inicio;

import org.apache.log4j.Logger;
import org.apache.commons.lang3.StringUtils;

public class App {

	final static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String msg = "clave.cl";
		
		App obj = new App();
		obj.runMe(msg);
		
		System.out.println("Con SpringUtils Capitalizado: "+StringUtils.capitalize(msg));
	}

	private void runMe(String parameter) {

		if (logger.isDebugEnabled()) {
			logger.debug("This is debug : " + parameter);
		}

		if (logger.isInfoEnabled()) {
			logger.info("This is info : " + parameter);
		}

		logger.warn("This is warn : " + parameter);
		logger.error("This is error : " + parameter);
		logger.fatal("This is fatal : " + parameter);

	}

}
